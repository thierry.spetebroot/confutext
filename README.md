# ConfuText (Confused Text)

A tool to generate typos in texts.

The aim of ConfuText (Confused Text) is to generate realistic inputs for natural language based interfaces (e.g., forms)
and machine learning algorithms (e.g., extend training sets, build test sets).

## Setup

ConfuText is a Python utility.

Dependencies are tracked via [`pipenv`](https://github.com/pypa/pipenv).

## Quickstart

```shell
pipenv install
pipenv run python -m confutext

pipenv run python -m confutext --dup 20 /path/to/input/file

```

## CLI

`ctxt` is the default ConfuText alias.

### Supported modifiers

Different type of errors should be handled:

 - character duplication (dup)
 - extra characters based on keyboard layouts
 - switch characters based on phonetics (see Soundex or Double-Metaphone)
 - swap characters order (swap)
 - punctuation 
 - dropout (missing characters) (drop)
 - synonym
