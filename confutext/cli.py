# -*- coding: utf-8 -*-
"""Main `confutext` CLI."""

import os
import sys
import random
import click

from typing import TextIO
from confutext import __version__

def version_msg():
    """Return the ConfuText version, location and Python powering it."""
    python_version = sys.version[:3]
    location = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
    message = u'ConfuText %(version)s from {} (Python {})'
    return message.format(location, python_version)

@click.command()
@click.version_option(__version__, u'-V', u'--version', message=version_msg())
@click.option('--dup', default=0, help='number of duplications')
@click.option('--swap', default=0, help='number of swaps')
@click.option('--drop', default=0, help='number of drops')
@click.option('--it', default=0, help='italian foods')
@click.argument('inputfile', type=click.File('r'))
@click.option('--out', default='', help='output file')
def main(inputfile:TextIO, dup, swap, drop, it, out):
    mem = "\n".join(inputfile.readlines())
    mem = duplication(mem, dup)
    mem = swaper(mem, swap)
    mem = droper(mem, drop)
    mem = italian(mem, it)
    if out:
        outfile = open(out, "w")
        outfile.write(mem)
        outfile.close()
    else:
        print(mem)
    return 0

if __name__ == '__main__':
    main()


def duplication(mem, n):
    for i in range(n) :
        index = random.randrange(0, len(mem))
        mem = mem[:index] + mem[index] + mem[index] + mem[index + 1:]
    return mem

def swaper(mem, n):
    for i in range(n):
        index = random.randrange(0, len(mem) - 1)
        mem = mem[:index] + mem[index + 1] + mem[index] + mem[index + 2:]
    return mem


def droper(mem, n):
    for i in range(n):
        index = random.randrange(0, len(mem) - 1)
        mem = mem[:index] + mem[index + 1:]
    return mem


def italian(mem, n):
    reserved_words = ["moza", "parmigiano", "pizza", "lasagna", "pandoro"]
    for i in range(n):
        index = random.randrange(0, len(mem) - 1)
        index_of_word = random.randrange(0, len(reserved_words))
        mem = mem[:index] + ' ' + reserved_words[index_of_word] + ' ' + mem[index:]
    return mem
