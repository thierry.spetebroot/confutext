# -*- coding: utf-8 -*-
"""Allow ConfuText to be executable from a checkout or zip file."""

import runpy


if __name__ == "__main__":
    runpy.run_module("confutext", run_name="__main__")
